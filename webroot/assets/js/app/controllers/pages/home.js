// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                var context = this;

                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/bg.jpg",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/btn-submit.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/checked.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/nocheck.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/close.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/home/formSprite.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/bg.jpg",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/name.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/text.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/city.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/result/time.png",
                            "size": 1
                        },
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                    }
                });

                // 参数
                app.baseUrl = 'http://campaign.imarmot.net'; // 域名

                var isAccess = 0; // 是否授权，默认没有
                app.openid = localStorage.openid || wxr_getParameterByName('openid');
                if(!app.openid) {
                    // 没有openid，调取接口
                    getAccess(); 
                } else {
                    // 已授权
                    isAccess = 1;
                    localStorage.openid = app.openid;
                    // 验证是否已经预约报名接口
                    checkOpenid();
                };

                // 获取授权接口
                function getAccess() {
                    $.ajax({
                        type: 'get',
                        url: app.baseUrl + '/ws-api/getopenid',
                        dataType: 'json',
                        data: {
                            url: app.localUrl
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(app.param == '' || app.param == undefined || app.param == null) {
                                // 没有参数
                                location.href = data.url;
                            } else {
                                // 有参数
                                location.href = data.url + "&" + app.param;
                            };
                        },
                        error: function() {
                            //
                            // alert('网络超时，请刷新再试');
                        },
                        complete: function(data) {
                            //
                            console.log(app.openid);
                        }
                    });
                };
                
                // 验证是否已经预约报名接口
                function checkOpenid() {
                    $.ajax({
                        type: 'get',
                        url: app.baseUrl + '/ws-api/checkopenid',
                        dataType: 'json',
                        data: {
                            openid: app.openid
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.code == '000') {
                                // 没预约过
                                //
                            } else if(data.code == '001') {
                                // 已经预约过
                                // alert(data.msg);
                                app.userName = data.body.userName;
                                app.userCity = data.body.userCity;
                                app.userTime = data.body.userTime;
                                // 去到结果页
                                tl.kill();
                                app.router.goto('result');
                            } else {
                                // 
                            }
                        },
                        error: function() {
                            //
                        },
                        complete: function(data) {
                            //
                        }
                    });
                }; 

                // 填充城市接口
                function fillCity() {
                    $('#defaultCity').siblings().remove();
                    $.ajax({
                        type: 'get',
                        url: app.baseUrl + '/ws-api/getaccount',
                        dataType: 'json',
                        data: {},
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            app.cityArr = data.body;
                            if(data.code == '001') {
                                // 填充城市
                                var len = data.body.length;
                                for(var i = 0; i < len; i++) {
                                    if(checkActivityTime(i)) {
                                        // 时间未过期
                                        $('.userCityVal').append('<option value=' + i + ' data-city=' + data.body[i].userCity + '>' + data.body[i].userCity + '</option>');
                                    } else {
                                        // 时间已过期
                                        $('.userCityVal').append('<option value=' + i + ' data-city=' + data.body[i].userCity + ' disabled>' + data.body[i].userCity + '</option>');
                                    }
                                }

                            } else {
                                //
                                alert('网络超时，请刷新再试');
                            }
                        },
                        error: function() {
                            //
                            alert('网络超时，请刷新再试');
                        },
                        complete: function(data) {
                            //
                        }
                    });
                };
                fillCity();

                // 检查活动时间是否过期
                function checkActivityTime(num) {
                    var today = new Date();
                    if(today.getFullYear() == 2019) {
                        // 2019年
                        switch(num) {
                            case 0:
                                // 南京基德 4-21
                                if((today.getMonth() + 1) <= 4) {
                                    // 4月
                                    if(today.getDate() > 21) {
                                        // 超过4月21日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else {
                                    return false;
                                };
                                break;
                            case 1:
                                // 长沙平和堂 5-1
                                if((today.getMonth() + 1) == 5) {
                                    // 5月
                                    if(today.getDate() > 1) {
                                        // 超过5月1日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 5) {
                                    return true;
                                } else {
                                    return false;
                                }
                                break;
                            case 2:
                                // 沈阳中兴 5-4
                                if((today.getMonth() + 1) == 5) {
                                    // 5月
                                    if(today.getDate() > 4) {
                                        // 超过5月4日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 5) {
                                    return true;
                                } else {
                                    return false;
                                }
                                break;
                            case 3:
                                // 上海久光
                                if((today.getMonth() + 1) == 5) {
                                    // 5月
                                    if(today.getDate() > 21) {
                                        // 超过5月21日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 5) {
                                    return true;
                                } else {
                                    return false;
                                };
                                break;
                            case 4:
                                // 成都伊势丹
                                if((today.getMonth() + 1) == 5) {
                                    // 5月
                                    if(today.getDate() > 21) {
                                        // 超过5月21日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 5) {
                                    return true;
                                } else {
                                    return false;
                                };
                                break;
                            case 5:
                                // 武汉广场
                                if((today.getMonth() + 1) == 6) {
                                    // 6月
                                    if(today.getDate() > 9) {
                                        // 超过6月9日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 6) {
                                    return true;
                                } else {
                                    return false;
                                };
                                break;
                            case 6:
                                // 南宁百盛
                                if((today.getMonth() + 1) == 6) {
                                    // 6月
                                    if(today.getDate() > 23) {
                                        // 超过6月18日
                                        return false;
                                    } else {
                                        return true;
                                    }
                                } else if((today.getMonth() + 1) < 6) {
                                    return true;
                                } else {
                                    return false;
                                };
                                break;
                            default:
                            //
                        }
                    } else {
                        // 整个活动都过期
                        return false;
                    }
                };

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.home_page'), 0.2, { autoAlpha: 0, onStart:function() {
                    // 
                }, onComplete: function() {
                    //
                } }, 0.1);

                //解决弹出输入法页面无法收回的问题
                $(document).on('focusin', function() {               
                    // alert('弹出')
                });

                $(document).on('focusout', function() {
                    // 
                    $("body").scrollTop($('body')[0].scrollHeight);
                });

                // 填充时间
                function fillTime(index) {
                    $('#defaultTime').siblings().remove();
                    if($('#userCityVal').val() == '') {
                        return;
                    };
                    var len = app.cityArr[index].dataList.length;
                    for(var i = 0; i < len; i++) {
                        if(app.cityArr[index].dataList[i].num < 220) {
                            // 可预约
                            $('.userTimeVal').append('<option value=' + i + ' data-time=' + app.cityArr[index].dataList[i].userTime + '>' + app.cityArr[index].dataList[i].userTime + '</option>');
                        } else {
                            // 不可预约
                            $('.userTimeVal').append('<option value=' + i + ' data-time=' + app.cityArr[index].dataList[i].userTime + ' disabled>' + app.cityArr[index].dataList[i].userTime + '</option>');
                        }
                    }
                };

                // 切换时间
                $('#userCityVal').on('change', function() {
                    fillTime($(this).val());
                });

                // 检查名字
                function checkName(name) {
                    var reg = /^[\u4e00-\u9fa5a-zA-Z]{2,16}$/;
                    if (!reg.test(name)) {
                        alert('名字格式不正确')
                        return false;
                    } else {
                        return true;
                    }
                };

                // 检查号码
                function checkNumber(number) {
                    if (number == '') {
                        alert('请输入手机号');
                        return;
                    }
                    if (!/^1[0-9][0-9]\d{8}$/.test(number)) {
                        alert('请输入正确的电话号码');
                        return false;
                    } else {
                        return true;
                    }
                };

                // 检查验证码
                function checkCode(code, length) {
                    if (code == '') {
                        alert('请输入验证码');
                        return false;
                    } else if(code.length != length){
                        alert('验证码错误')
                        return false;
                    } else {
                        return true;
                    }
                };

                // 检查城市
                function checkCity(city) {
                    if(city == undefined || city == '') {
                        alert('请选择城市');
                        return false;
                    } else {
                        return true;
                    }
                };

                // 检查时间
                function checkTime(time) {
                    if(time == undefined || time == '') {
                        alert('请选择日期');
                        return false;
                    } else {
                        return true;
                    }
                };

                // 获取验证码按钮
                $('.send').off('click');
                $('.send').on('click', function() {
                    if(isAccess == 0) {
                        // 未授权
                        alert('未授权');
                        return;
                    } else {
                        // 已授权
                        if(checkNumber($('#userPhoneVal').val())) {
                            $('.send').hide();
                            timeCountDown($('.seconds'), 60);
                            getCode();
                        }
                    }
                });

                // 倒计时
                function timeCountDown(elm, t) {
                    elm.show();
                    elm.text(t);
                    var timeCountDownFun = setInterval(function() {
                        if(t == 0) {
                            elm.hide();
                            $('.send').show();
                            elm.text('');
                            clearInterval(timeCountDownFun);
                            console.log('time countdown done');
                        } else {
                            t--;
                            elm.text(t);
                        }
                    }, 1000);
                };

                // 获取短信验证码接口
                function getCode() {
                    $.ajax({
                        type: 'get',
                        url: app.baseUrl + '/ws-api/verifycode',
                        dataType: 'json',
                        data: {
                            userNumber: $('#userPhoneVal').val()
                        },
                        // async: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            if(data.code == '000') {
                                // 没预约过
                                alert(data.message);
                            } else if(data.code == '001') {
                                // 已经预约过
                                alert(data.message);
                                app.userName = data.body.userName;
                                app.userCity = data.body.userCity;
                                app.userTime = data.body.userTime;
                                // 去到结果页
                                tl.kill();
                                app.router.goto('result');
                            } else {
                                // 
                            }
                        },
                        error: function() {
                            //
                        },
                        complete: function(data) {
                            //
                        }
                    });
                };

                // checkbox
                $('.checkbox').on('click', function() {
                    if($('.agreement').data('check')) {
                        $('.agreement').data('check', false);
                        $('.text').attr('src', 'assets/images/home/nocheck.png');
                    } else {
                        $('.agreement').data('check', true);
                        $('.text').attr('src', 'assets/images/home/checked.png');
                    }
                });

                // 隐私政策
                $('.rules').off('click');
                $('.rules').on('click', function() {
                    $('.modal').fadeIn('fast');
                });

                // 关闭隐私政策
                $('.btn-close').on('click', function() {
                    $('.modal').fadeOut('fast');
                });

                // submit
                var canSubmit = 1; // 是否可以点击提交，默认可以
                $('.btn-submit').off('click');
                $('.btn-submit').on('click', function() {
                    if(isAccess == 0) {
                        // 未授权
                        alert('未授权');
                        return;
                    } else {
                        gtag('event', 'submit', {'event_category': 'CPB', 'event_label': 'click'});
                        // 已授权
                        app.userName = $('#userNameVal').val();
                        app.userNumber = $('#userPhoneVal').val();
                        app.userCode = $('#userCodeVal').val();
                        app.userCity = $('#userCityVal option:selected').data('city');
                        app.userTime = $('#userTimeVal option:selected').data('time');
                        // console.log(app.userName);
                        // console.log(app.userNumber);
                        // console.log(app.userCode);
                        // console.log(app.userCity);
                        // console.log(app.userTime);
                        if(checkName(app.userName) && checkNumber(app.userNumber) && checkCode(app.userCode, 6) && checkCity(app.userCity) && checkTime(app.userTime)) {
                            if($('.agreement').data('check')) {
                                if(canSubmit == 0) {
                                    // 正在调接口，不可以重复调取
                                    return;
                                };
                                canSubmit = 0;
                                // 同意
                                $.ajax({
                                    type: 'post',
                                    url: app.baseUrl + '/ws-api/submituser',
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    data: JSON.stringify({
                                        openid: app.openid,
                                        userName: app.userName,
                                        userNumber: app.userNumber,
                                        userCode: app.userCode,
                                        userCity: app.userCity,
                                        userTime: app.userTime
                                    }),
                                    // async: false,
                                    cache: false,
                                    success: function(data) {
                                        console.log(data);
                                        alert(data.message);
                                        if(data.code == '001') {
                                            // 成功
                                            app.userName = app.userName,
                                            app.userCity = app.userCity,
                                            app.userTime = app.userTime
                                            // 去到结果页
                                            tl.kill();
                                            app.router.goto('result');
                                        }
                                    },
                                    error: function() {
                                        //
                                        alert('提交失败，请重试');
                                    },
                                    complete: function(data) {
                                        //
                                        canSubmit = 1;
                                    }
                                });
                            } else {
                                // 不同意
                                alert('需同意隐私政策');
                            }
                        }
                    }
                });

                // 营业执照
                $('.license1').on('click', function() {
                    // 去到营业执照页
                    tl.kill();
                    // app.router.goto('article-duliye-223', ['home']);
                    app.router.goto('article-duliye-223');
                });
            },
            resize: function(ww, wh) {
                // alert(ww+''+wh);
                if(wh/ww <= 1.51){
                    // 5s带bar
                    // alert('wh/ww < 1.51');
                    // $('.form-box').css('top', '6%');
                };
                if(wh/ww >1.51 && wh/ww <= 1.63){
                    // 5s不带bar, 6s不带bar
                    // alert('wh/ww >1.51 && wh/ww <= 1.63');
                    // $('.form-box').css('top', '8%');
                };
                if(wh/ww >1.63 && wh/ww <= 1.76){
                    // x带bar
                    // alert('wh/ww >1.63 && wh/ww <= 1.76');
                    //
                };
                if(wh/ww > 1.76 && wh/ww < 1.9){
                    // 小米测试机
                    // alert('wh/ww > 1.76 && wh/ww < 1.9');
                    //

                };
                if( wh/ww > 1.9){
                    // x不带bar
                    // alert('wh/ww > 1.9');
                    //
                }
            },
        })
        //  Return the module for AMD compliance.
        return Page;
    })