require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        //微信分享
        app.shareUrl = 'http://campaign.imarmot.net/cpb_radiance_reservation';

        app.localUrl = window.location.href; // 当前url
        app.param = app.localUrl.split('?')[1];
        if(app.param == '' || app.param == undefined || app.param == null) {
            // 不带参数
            app.shareLink = app.shareUrl;
            console.log('不带参数的' + app.shareLink);
        } else {
            // 带了参数
            if(app.param.indexOf('openid') != -1) {
                // 带了openid
                app.shareLink = app.shareUrl;
                console.log('带了openid的' + app.shareLink);
            } else {
                // 不带openid
                if(/utm_source=(nanjing|shanghai|chengdu|changsha|shenyang|wuhan|nanning){1}&utm_medium=city&utm_campaign=CPB/g.test(app.param)) {
                    // 正确的参数
                    app.shareLink = app.shareUrl + '/?' + app.param;
                    console.log('正确的' + app.shareLink);
                } else {
                    // 错误的参数
                    app.shareLink = app.shareUrl;
                    console.log('错误的' + app.shareLink);
                }
            }
        };

        app.shareDataFriend = {
            share: {
                    title: '你，会是下一位光耀女神吗？',
                    desc: 'CPB晶钻奢享展 | 七大城市寻觅光耀女神',
                    link: app.shareLink,
                    imgUrl: app.shareUrl + '/assets/images/share.jpg',
                    success: function() {
                        //
                    }
                }
        };
        app.shareDataTimeLine = {
            share: {
                    title: '你，会是下一位光耀女神吗？',
                    desc: 'CPB晶钻奢享展 | 七大城市寻觅光耀女神',
                    link: app.shareLink,
                    imgUrl: app.shareUrl + '/assets/images/share.jpg',
                    success: function() {
                        //
                    }
                }
        };

        if (window.location.host.indexOf('campaign.imarmot.net') != -1) {
            // require(['//res.wx.qq.com/open/js/jweixin-1.0.0.js'],
            // function(wx) {
            //     app.wx = wx;
            //     $.ajax({
            //         type: 'get',
            //         url: 'http://campaign.imarmot.net/ws-api/getwxshare',
            //         data: { url: app.shareLink },
            //         cache: false,
            //         success: function(data) {
            //             alert(JSON.stringify(data));
            //             if(data.is_success) {
            //                 app.wx.config({
            //                     "debug": true,
            //                     "appId": data.appid,
            //                     "timestamp": data.timestamp,
            //                     "nonceStr": data.nonceStr,
            //                     "signature": data.signature,
            //                     "jsApiList": ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"]
            //                 });
            //                 app.wx.ready(function() {
            //                     alert(app.wx);
            //                     app.wx.onMenuShareAppMessage(app.shareDataFriend.share);
            //                     app.wx.onMenuShareTimeline(app.shareDataTimeLine.share);
            //                 });
            //             } else {
            //                 // alert(data.err_msg);
            //             }
            //         },
            //         error: function(XHR, textStatus, errorThrown) {
            //             //
            //         }
            //     });
            // });

            (function() {
                //正式环境获取微信接口
                $.ajax({
                    type: 'get',
                    url: 'http://campaign.imarmot.net/ws-api/api/public/wechat/getWxConfig',
                    data: { url: window.location.href },
                    cache: false,
                    success: function(config) {
                        JSON.stringify(config)
                        var wxConfig = {
                            debug: false,
                            appId: config.body.appid,
                            timestamp: config.body.timestamp,
                            nonceStr: config.body.nonceStr,
                            signature: config.body.signature,
                            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo']
                        };
                        wx.config(wxConfig);
                    },
                    error:function(config) {
                        /* Act on the event */
                        //
                    }
                });

                //绑定微信分享事件
                try {
                    wx.ready(function() {
                        //设置分享后的回调函数
                        var callback = {
                            success: function(msg) {
                                msg = JSON.stringify(msg).toLowerCase();
                                if (msg.indexOf('timeline') > -1) {
                                    //分享到朋友圈
                                } else if (msg.indexOf('message') > -1) {
                                    //分享到给朋友
                                }
                            }
                        };
                        wx.onMenuShareTimeline($.extend({}, app.shareDataFriend.share, callback));
                        wx.onMenuShareAppMessage($.extend({}, app.shareDataTimeLine.share, callback));
                    });
                } catch (e) {
                    var msg = '错误：微信js-sdk未引用或者错误!';
                    try {
                        console.log(msg);
                    } catch (e) {
                        alert(msg);
                    }
                    return;
                }
            })();
        }
    });